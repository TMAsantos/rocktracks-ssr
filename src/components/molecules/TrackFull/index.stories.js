import React from 'react'
import { storiesOf, addDecorator } from '@storybook/react'
import TrackFull from '../TrackFull'

const track = {
  trackId: 279812226,
  coverUrl: 'https://is4-ssl.mzstatic.com/image/thumb/Music/v4/4f/0e/7e/4f0e7eda-daa7-ba7b-0e08-3058c4901c50/source/100x100bb.jpg',
  artist: 'artist 2',
  track: 'track 2',
  price: '10 $',
  duration: '10',
  releaseDate: '2000-01-01'
}

storiesOf('TrackFull', module)
  .add('default', () => (
    <TrackFull {...track}/>
  ))

/**/
