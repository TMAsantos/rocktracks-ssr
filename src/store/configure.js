import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import thunk from 'redux-thunk';

import reducer from './reducer'
import sagas from './sagas'

const configureStore = (initialState = {}, services = {}) => {
  const sagaMiddleware = createSagaMiddleware();

  const enhancers = [
      applyMiddleware(
        sagaMiddleware,
        thunk
      )
    ];

  const store = createStore(reducer, initialState, compose(...enhancers))
  let sagaTask = sagaMiddleware.run(sagas, services)

  return {
    ...store,
    runSaga: sagaMiddleware.run
  };
}

export default configureStore
