import React from 'react'
import { shallow, mount } from 'enzyme'
import { Provider } from "react-redux";
import { StaticRouter } from "react-router-dom";
import configureStore from "redux-mock-store";

import { getTracks } from './sagas.js'
import { call, put } from 'redux-saga/effects'

import { getTracksSuccess } from './actions'

const mockStore = configureStore();

import TrackList from '.'

const mockData = [
  {
    trackId: 0,
    coverUrl: 'https://is3-ssl.mzstatic.com/image/thumb/Music49/v4/d1/cd/45/d1cd453a-d19b-560c-a81f-b4a561794fd6/source/100x100bb.jpg',
    artist: 'Don Davis',
    track: 'Trinity Infinity',
    price: '1.51',
  },
  {
    trackId: 1,
    coverUrl: 'https://test.com/test.jpg',
    artist: 'Juno Reactor',
    track: 'Navras',
    price: '1.50',
  }
]

describe('TrackList component', () => {
  it('should render', () => {
    let store = mockStore({TrackList:{ isLoading: true }});
    const wrapper = shallow(<TrackList tracks={mockData} />,{ context: { store } });

    expect(wrapper).toMatchSnapshot();
  })

  it('should show 2 track summaries', () => {
    let store = mockStore({TrackList:{ isLoading: true }});
    const wrapper = mount(<Provider store={store}><StaticRouter context={{}}><TrackList tracks={mockData}/></StaticRouter></Provider>);

    expect(wrapper.find('TrackSummary').length).toBe(2);
  })

  it('should show 2 track summaries is they come from the store', () => {
    let store = mockStore({TrackList:{ isLoading: true, tracks: mockData }});
    const wrapper = mount(<Provider store={store}><StaticRouter context={{}}><TrackList /></StaticRouter></Provider>);

    expect(wrapper.find('TrackSummary').length).toBe(2);
  })

  it('should have getTracks saga working', () => {
    const api = { getTracks:() => (1) }
    const gen = getTracks( api )

    expect(gen.next().value).toEqual(call(api.getTracks))
    expect(gen.next(1).value).toEqual(put(getTracksSuccess(1)))
  })

})
