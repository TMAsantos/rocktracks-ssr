import { fork } from 'redux-saga/effects'

import trackListSagas from '../components/organisms/TrackList/sagas.js'

const sagas = [ trackListSagas ]

export default function* (services = {}) {
  yield fork(trackListSagas, services)
}
