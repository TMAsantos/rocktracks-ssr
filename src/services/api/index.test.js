
import { shallow, mount } from 'enzyme'

import getTracks from './get-tracks.js'

const mockResults = [
  {
    trackId: 1,
    artworkUrl100: 'https://testUrl',
    artworkUrl60: 'https://testUrl2',
    trackViewUrl: 'https://testUrl3',
    artistName: 'The Beatles',
    trackName: 'Hello, Goodbye',
    trackPrice: 1.23,
    releaseDate: '2008-06-23T07:00:00Z',
    trackTimeMillis: 123111
  }
]

const mockFetch = () => Promise.resolve({
  json: () => Promise.resolve({
      results: mockResults
    })
})

describe('Get Tracks api', () => {
  it('should parse data coming from a remote source', done => {
    getTracks(mockFetch)()
    .then(res => {
      expect(res.length).toBe(1)
      expect(res).toEqual([{"artist": "The Beatles", "coverUrl": "https://testUrl", "duration": "2.05", "price": "1.23 $", "releaseDate": "2008-06-23", "smallCoverUrl": "https://testUrl2", "track": "Hello, Goodbye", "trackId": 1, "trackViewUrl": "https://testUrl3"}])
      done()
    })
  })
})
