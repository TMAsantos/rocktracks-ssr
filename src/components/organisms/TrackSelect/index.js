import React from "react";
import { connect } from 'react-redux'

import Card from '../../atoms/Card'

import {
  getTracks,
  selectTrack
} from './actions'

import TrackFull from '../../molecules/TrackFull'

class TrackSelect extends React.Component {
  componentDidMount( ) {
    if(!this.props.tracks){
      this.props.getTracks()
    } else {
      this.props.selectTrack(this.props.trackId)
    }
  }

  render( ) {
    if(this.props.error)
      return (<h2>There was an error. Does the track exist?</h2>)
    return ( <TrackFull {...this.props.track}/> )
  }
}

const mapStateToProps = ({TrackSelect}, props) => {
  const toReturn = {
    ...props,
    ...TrackSelect
  }
  return toReturn
}

const mapDispatchToProps = (dispatch) => ({
  getTracks: () => dispatch(getTracks()),
  selectTrack: id => dispatch(selectTrack(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(TrackSelect)
