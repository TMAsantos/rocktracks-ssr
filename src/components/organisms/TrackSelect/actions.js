export const GET_TRACKS_REQUEST = 'TrackList/GET_TRACKS_REQUEST'
export const GET_TRACKS_SUCCESS = 'TrackList/GET_TRACKS_SUCCESS'
export const SELECT_TRACK_REQUEST = 'TrackSelect/SELECT_TRACK_REQUEST'

export function getTracks(){
  return {
    type: GET_TRACKS_REQUEST
  }
}

export function selectTrack(id){
  return {
    type: SELECT_TRACK_REQUEST,
    id
  }
}
