import React from 'react'
import { storiesOf, addDecorator } from '@storybook/react'
import Card from '../Card'

storiesOf('Card', module)
  .add('default', () => (
    <Card>Test card content</Card>
  ))

/**/
