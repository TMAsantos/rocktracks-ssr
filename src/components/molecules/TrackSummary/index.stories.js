import React from 'react'
import { storiesOf, addDecorator } from '@storybook/react'
import TrackSummary from '../TrackSummary'

storiesOf('TrackSummary', module)
  .add('default', () => (
    <TrackSummary
      coverUrl = {'https://is3-ssl.mzstatic.com/image/thumb/Music49/v4/d1/cd/45/d1cd453a-d19b-560c-a81f-b4a561794fd6/source/100x100bb.jpg'}
      artist = {'artist'}
      track = {'track'}
      price = {'price'}
    />
  ))

/**/
