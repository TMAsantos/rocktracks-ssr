const dateFormat = require('dateformat');

const getTracks = fetch => () => {
  return fetch('https://itunes.apple.com/search?term=rock&media=music')
  .then(res => res.json())
  .then(res => res.results.map(result => ({
    trackId: result.trackId,
    coverUrl: result.artworkUrl100,
    smallCoverUrl: result.artworkUrl60,
    trackViewUrl: result.trackViewUrl,
    artist: result.artistName,
    track: result.trackName,
    price: `${result.trackPrice} $`,
    releaseDate: dateFormat(Date.parse(result.releaseDate), "yyyy-mm-dd"),
    duration: (result.trackTimeMillis / 1000 / 60).toFixed(2)
  })))
  .catch(error => {
    console.log('error!!', error)
  })
}

export default getTracks
