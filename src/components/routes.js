import Home from "./pages/Home";
import Track from "./pages/Track";

export default [
    {
        path: "/",
        component: Home,
        exact: true
    },
    {
        path: "/track",
        component: Home,
        exact: true
    },
    {
        path: "/track/:trackId",
        component: Track
    }
];
