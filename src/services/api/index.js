import fetch from "isomorphic-fetch";

import getTracks from './get-tracks.js'

const apis = {
  getTracks: getTracks(fetch)
}

export default apis
