import React from "react";
import { connect } from 'react-redux'
import { Link } from "react-router-dom";

import {
  getTracks
} from './actions'

import TrackSummary from '../../molecules/TrackSummary'

class TrackList extends React.Component {
  componentDidMount( ) {
    if ( !this.props.tracks ) {
      this.props.getTracks();
    }
  }

  render( ) {
    return (
      <div style={{display:'flex', flexDirection:'row', flexWrap: 'wrap'}}>
        {
          this.props.tracks
          && this.props.tracks.map(track => (
            <span style={{"display": "block"}} key={track.trackId}>
              {/*Added Link instead of onClick so that the client is able to redirect before the bundle is downloaded*/}
              <Link to={'/track/'+track.trackId} style={{ color: '#000' }}>
                <TrackSummary {...track} />
              </Link>
            </span>
          ))
        }
      </div>
    )
  }
}

const mapStateToProps = ({TrackList}, props) => {
  const toReturn = {
    ...props,
    ...TrackList
  }
  return toReturn
}

const mapDispatchToProps = (dispatch) => ({
  getTracks: () => dispatch(getTracks())
})

export default connect(mapStateToProps, mapDispatchToProps)(TrackList)
