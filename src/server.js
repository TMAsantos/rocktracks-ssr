import express from "express";
import path from "path";

import React from "react";
import { renderToString } from "react-dom/server";
import { StaticRouter, matchPath } from "react-router-dom";
import { Provider } from "react-redux";
import api from './services/api'

import App from "./components/App";
import configureStore from './store/configure';
import routes from './components/routes'

const app = express();

app.use( express.static( path.resolve( __dirname, "../dist" ) ) );

app.get( "/*", ( req, res ) => {
  const store = configureStore({}, { api })

  const preFetchData =
        routes
        .filter( route => matchPath( req.url, route ) )
        .map( route => route.component )
        .filter( component => component.preFetchData )
        .map( component => component.preFetchData(store, api, req.url) );

  Promise.all( preFetchData ).then( promres => {

    const jsx = (
      <Provider store={ store }>
        <StaticRouter context={ { } } location={ req.url }>
          <App />
        </StaticRouter>
      </Provider>
    );
    const reactDom = renderToString( jsx );

    const reduxState = store.getState( );

    res.writeHead( 200, { "Content-Type": "text/html" } );
    res.end( htmlTemplate( reactDom, reduxState ) );
  } );
} );

app.listen( 3000 );

function htmlTemplate( reactDom, reduxState ) {
    return `
        <!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            <title>Rock Tracks</title>
        </head>

        <body>
            <div id="app">${ reactDom }</div>
            <script>
                window.REDUX_DATA = ${ JSON.stringify( reduxState ) }
            </script>
            <script src="/app.bundle.js"></script>
        </body>
        </html>
    `;
}
