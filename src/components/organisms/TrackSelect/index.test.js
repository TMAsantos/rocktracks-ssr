import React from 'react'
import { shallow, mount } from 'enzyme'
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";

import { getTracksSuccess } from './actions'

const mockStore = configureStore();

import TrackSelect from '.'

import {
  GET_TRACKS_REQUEST,
  SELECT_TRACK_REQUEST
} from './actions'

const mockData = [
  {
    trackId: 0,
    coverUrl: 'https://is3-ssl.mzstatic.com/image/thumb/Music49/v4/d1/cd/45/d1cd453a-d19b-560c-a81f-b4a561794fd6/source/100x100bb.jpg',
    artist: 'Don Davis',
    track: 'Trinity Infinity',
    price: '1.51',
  },
  {
    trackId: 1,
    coverUrl: 'https://test.com/test.jpg',
    artist: 'Juno Reactor',
    track: 'Navras',
    price: '1.50',
  }
]

const mockTrack = {
  trackId: 1,
  coverUrl: 'https://test.com/test.jpg',
  artist: 'Juno Reactor',
  track: 'Navras',
  price: '1.50',
}

describe('TrackList component', () => {
  it('should render', () => {
    let store = mockStore({TrackSelect:{ isLoading: true }});
    const wrapper = shallow(<TrackSelect tracks={mockData} track={mockTrack}/>,{ context: { store } });

    expect(wrapper).toMatchSnapshot();
  })

  it('should show the selected track', () => {
    let store = mockStore({TrackSelect:{ isLoading: true }});
    const wrapper = mount(<Provider store={store}><TrackSelect tracks={mockData} track={mockTrack}/></Provider>);

    expect(wrapper.find('TrackFull').length).toBe(1);
  })

  it('should show an error message is no track was found', () => {
    let store = mockStore({TrackSelect:{ isLoading: true }});
    const wrapper = mount(<Provider store={store}><TrackSelect tracks={mockData} error={true}/></Provider>);

    expect(wrapper.find('TrackFull').length).toBe(0);
    expect(wrapper.find('h2').length).toBe(1);
    expect(wrapper.find('h2').props().children).toBe("There was an error. Does the track exist?");
  })

  it('should dispatch a getTracks action if no tracks are provided', () => {
    let store = mockStore({TrackSelect:{ isLoading: true }});
    const wrapper = mount(<Provider store={store}><TrackSelect /></Provider>);

    expect(store.getActions().length).toBe(1);
    expect(store.getActions()[0].type).toBe(GET_TRACKS_REQUEST);
  })

  it('should dispatch a selectTrack action if tracks are provided', () => {
    let store = mockStore({TrackSelect:{ isLoading: true }});
    const wrapper = mount(<Provider store={store}><TrackSelect tracks={mockData} /></Provider>);

    expect(store.getActions().length).toBe(1);
    expect(store.getActions()[0].type).toBe(SELECT_TRACK_REQUEST);
  })
})
