import React from "react";
import { Link } from "react-router-dom";

import TrackSelect from '../../organisms/TrackSelect/index.js'

import { getTracks } from '../../organisms/TrackList/sagas'
import { selectTrack } from '../../organisms/TrackSelect/actions'

const Track = ({match}) => {
  return (
    <div>
      <div>
          <Link to="/">Back Home</Link>
      </div>
      <TrackSelect trackId={match.params.trackId}/>
    </div>
  )
};

//This component can pre fetch data server side
Track.preFetchData = (store, api, params) => {
  return store.runSaga(getTracks, api)
  .toPromise()
  .then( () => {
    //TODO this has to be improved :)
    const trackId = params.substr(7, params.length)
    return store.dispatch(selectTrack(trackId))
  } )
};

export default Track;
