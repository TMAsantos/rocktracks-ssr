import React from "react";

const style = {
  background: '#fff',
  borderRadius: '2px',
  display: 'inline-block',
  margin: '1rem',
  padding: '0rem',
  position: 'relative',
  boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)'
}

const Card = props => (
    <div style={style}>
      {props.children}
    </div>
);

export default Card;
