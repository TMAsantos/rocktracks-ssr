import React from "react";

import TrackList from '../../organisms/TrackList/index.js'

import { getTracks } from '../../organisms/TrackList/sagas'

class Home extends React.Component {
    render( ) {
      return (
        <div>
          <h2>Choose your track:</h2>
          <TrackList {...this.props}/>
        </div>
      );
    }
}

//This component can pre fetch data server side
Home.preFetchData = (store, api, params) => {
  return store.runSaga(getTracks, api)
  .toPromise()
};;

export default Home
