import React from "react";
import { Link, Switch, Route } from "react-router-dom";
import routes from './routes'

export default class App extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div>
                <h1>Rock Tracks</h1>
                <Switch>
                    { routes.map( route => <Route key={ route.path } { ...route } /> ) }
                </Switch>
            </div>
        );
    }
}
