# Rock Track - React Server Side Rendering

*Tiago Santos - February 2019*

The Rock Track project is a simple implementation of React SSR.   
The goal is to fetch data from `https://itunes.apple.com/search?term=rock&media=music` and serve it to the user on 2 different pages. The first page shows the list of all tracks and the second shows the details of a selected track.

## Used Tech and Features
1. React 16;
2. React-Redux for state management;
3. Redux-Sagas and Redux-Thunk for async tasks and data prefetching on the server;
4. Testing using jest and enzyme;
5. Storybooks to preview the components;
6. Docker for easy deployment.

# Requirements

This project was developed using `node 8.10.0`. It was tested and developed on `Ubuntu 16`, `Chrome` and `Firefox`.

# Installation

1. Clone this project
2. Install the dependencies: `npm install`

# Testing

1. Run: `npm run test`

# Running

## Development

1. Start: `npm start`
2. Visit: `http://localhost:3000`

## Production

1. Build: `npm run build`
2. Start server: `node index.js`
3. Visit: `http://localhost:3000`

### Docker

1. Build: `docker build -t rock_tracks .`
2. Run: `docker run --name rock_tracks -p 3000:3000 -d rock_tracks`
3. Visit: `http://localhost:3000`

# Components

Component structure follows Atomic React guidelines: https://github.com/diegohaz/arc

* `Card`: Simple material design card component to put data inside for display;
* `TrackFull`: Displays a set of track properties;
* `TrackSummary`: Displays a smaller set of track properties;
* `TrackList`: Shows a set of TrackSummary components based on the tracks that it receives as a property or redux;
* `TrackSelect`: Selects to show a TrackFull component by its trackId, from a set of tracks that is receives from either a prop or redux;
* `Track`: Page that shows a track's full information;
* `Home`: Home page. Shows a list of tracks by rendering the TrackList component.

For more information please see their storybooks.

# Things to Note

## Data prefetching

Page components such as Home and Track, have a preFetchData property (function).   
This function fetches data (via redux-sagas) and dispatches redux actions server-side. It is called before sending the response to the client app. This way, the client is not served with a "blank" HTML page while it waits for the bundle to download.
The preFetchData function can be implemented differently per page component, since each page has different prefetching requirements.

Deciding which data should be fetched server-side (prefetching) vs client-side (lazy-loading) is important, since the user should receive feedback as soon as possible.
Due to the simplicity of this project I decided to test both types of fetching by doing prefetching of the tracks on the server-side and lazy-loading the track's pictures on the client-side. This works because the list of tracks is not that big.   
In scenarios where the data being fetched is too big, the user should be served with a loading screen and the data should be lazy-loaded instead.  

## Slow network support

Use Google Chome's network feature and check how the Web App behaves on slow 3G networks.  
This could be further improved by code-splitting the bundle, so that the client is served with the JavaScript logic faster.

## Testability

Tools like redux and sagas improve state management of the application, but also its testability. You can find some test examples on the TrackList and TrackSelect components.

An example of dependency injection can also be seen on the `getTracks()` API. Note how easy it is to mock the isomorphic-fetch library at `src/services/api`.

## Storybooks

By running `npm run storybook` and visiting `http://localhost:9009` you can preview the components used in this project.

## Docker

You can find a `Dockerfile` on this project. This makes it easily deployable to production environments.

# TODOs
* Improve styling. There are better ways of handling css: https://www.styled-components.com/docs/basics
* More code-splitting to split the bundle like in: https://marmelab.com/blog/2017/10/17/code-splitting.html
* Add hot-reloading for faster development: https://webpack.js.org/guides/development/#using-webpack-dev-middleware
* Better error state handling for when API is not accessible
* Code linting can also be used.
