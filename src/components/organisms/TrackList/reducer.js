import {
  GET_TRACKS_REQUEST,
  GET_TRACKS_SUCCESS,
} from './actions'

const initialState = {
  isLoading: false,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_TRACKS_REQUEST:
      return {
        ...state
      }
    case GET_TRACKS_SUCCESS:
      return {
        ...state,
        tracks: action.tracks
      }
    default:
      return state
  }
}
