import React from "react";
import { connect } from 'react-redux'

import Card from '../../atoms/Card'

const style = {
  display: 'flex',
  flexDirection: 'column',
  width: '300px',
  alignItems: 'center',
  padding: '1rem'
}

class TrackFull extends React.Component {

  render( ) {
    return (
      <Card>
        <div style={style}>
          <div>
            <img src={this.props.coverUrl} />
          </div>
          <div>
            <ul>
              <li>Artist: {this.props.artist}</li>
              <li>Track: {this.props.track}</li>
              <li>Price: {this.props.price}</li>
              <li>Duration: {this.props.duration} minutes</li>
              <li>Released on: {this.props.releaseDate}</li>
            </ul>
            <button onClick={()=>{window.open(this.props.trackViewUrl, '_blank');}}>More details</button>
          </div>
        </div>
      </Card>
    )
  }
}

export default TrackFull
