import React from "react";

import Card from '../../atoms/Card'

const style = {
  display: 'flex',
  flexDirection: 'row'
}

const TrackSummary = props => (
  <Card>
    <div style={style}>
      <img src={props.coverUrl} />
      <div>
        <ul>
          <li>{props.artist}</li>
          <li>{props.track}</li>
          <li>{props.price}</li>
        </ul>
      </div>
    </div>
  </Card>
);

export default TrackSummary;
