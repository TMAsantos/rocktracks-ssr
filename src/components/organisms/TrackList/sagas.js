import { call, takeEvery, fork, put } from 'redux-saga/effects'

import {
  GET_TRACKS_REQUEST,
  getTracksSuccess
} from './actions'

const mockTracks = [
  {
    trackId: 279812225,
    coverUrl: 'https://is3-ssl.mzstatic.com/image/thumb/Music49/v4/d1/cd/45/d1cd453a-d19b-560c-a81f-b4a561794fd6/source/100x100bb.jpg',
    artist: 'artist 1',
    track: 'track 1',
    price: 1
  },
  {
    trackId: 279812226,
    coverUrl: 'https://is4-ssl.mzstatic.com/image/thumb/Music/v4/4f/0e/7e/4f0e7eda-daa7-ba7b-0e08-3058c4901c50/source/100x100bb.jpg',
    artist: 'artist 2',
    track: 'track 2',
    price: 10
  }
]

export function* getTracks(api, action) {
  try {
    const tracks = yield call(api.getTracks)

    yield put(getTracksSuccess(tracks))
  } catch (e) {
    console.log('sagas error!', e)
  }
}

export function* watch(api) {
  yield takeEvery(GET_TRACKS_REQUEST, getTracks, api)
}

export default function* ({api}) {
  yield fork(watch, api)
}
