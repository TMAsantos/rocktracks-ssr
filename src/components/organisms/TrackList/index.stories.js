import React from 'react'
import { storiesOf, addDecorator } from '@storybook/react'
import TrackList from '../TrackList'
import { StaticRouter } from "react-router-dom";

import rootReducer from './reducer'

//ref: https://medium.com/ingenious/storybook-meets-redux-6ab09a5be346
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux'
const store = createStore(combineReducers({TrackList:rootReducer}))
//import { withViewport } from '@storybook/addon-viewport';

const tracks = [
  {
    trackId: 279812225,
    coverUrl: 'https://is3-ssl.mzstatic.com/image/thumb/Music49/v4/d1/cd/45/d1cd453a-d19b-560c-a81f-b4a561794fd6/source/100x100bb.jpg',
    artist: 'artist 1',
    track: 'track 1',
    price: 1
  },
  {
    trackId: 279812226,
    coverUrl: 'https://is4-ssl.mzstatic.com/image/thumb/Music/v4/4f/0e/7e/4f0e7eda-daa7-ba7b-0e08-3058c4901c50/source/100x100bb.jpg',
    artist: 'artist 2',
    track: 'track 2',
    price: 10
  }
]

storiesOf('TrackList', module)
  .addDecorator(story => <Provider store={store}><StaticRouter context={{}}>{story()}</StaticRouter></Provider>)
  .add('default', () => (
    <TrackList tracks={tracks} />
  ))

/**/
