#docker build -t rock_tracks .
#docker run --name rock_tracks -p 3000:3000 -d rock_tracks

FROM node:8.10.0

# Create app directory
RUN mkdir -p /rock_tracks

COPY . /rock_tracks

#Set the working directory to here
WORKDIR /rock_tracks

#Install app dependencies
RUN npm install

# Build for production.
RUN npm run build

# Set the command to start the node server.
CMD node index.js

EXPOSE 3000
