import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import api from './services/api'

import App from "./components/App";
import configureStore from './store/configure';

const store = configureStore( window.REDUX_DATA, { api } );

const jsx = (
  <Provider store={ store }>
    <Router>
        <App />
    </Router>
  </Provider>
);

const app = document.getElementById( "app" );
ReactDOM.hydrate( jsx, app );
