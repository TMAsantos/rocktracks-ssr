import {
  GET_TRACKS_SUCCESS,
  SELECT_TRACK_REQUEST
} from './actions'

const initialState = {
  isLoading: false,
}

export default (state = initialState, action) => {
  //console.log('running reducer',action.type)
  switch (action.type) {
    case GET_TRACKS_SUCCESS:
      //console.log('track select got tracks!')
      return {
        ...state,
        tracks: action.tracks
      }
    case SELECT_TRACK_REQUEST:
      let track = state.tracks.filter(track => {
        return track.trackId == action.id
      })[0];

      if( !track ) return { ...state, error: true }
      return { ...state, track, error: false }
    default:
      return state
  }
}
