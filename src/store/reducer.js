import { combineReducers } from 'redux'

//TODO maki this more dynamic like in https://github.com/tiagosantos967/react_guide/

let reducers = { }

import trackListReducer from '../components/organisms/TrackList/reducer.js'
import trackSelectReducer from '../components/organisms/TrackSelect/reducer.js'
reducers['TrackList'] = trackListReducer
reducers['TrackSelect'] = trackSelectReducer

export default combineReducers(reducers)
