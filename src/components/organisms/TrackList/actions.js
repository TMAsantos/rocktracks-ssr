export const GET_TRACKS_REQUEST = 'TrackList/GET_TRACKS_REQUEST'
export const GET_TRACKS_SUCCESS = 'TrackList/GET_TRACKS_SUCCESS'

export function getTracks(){
  return {
    type: GET_TRACKS_REQUEST
  }
}

export function getTracksSuccess(tracks){
  return {
    type: GET_TRACKS_SUCCESS,
    tracks
  }
}
