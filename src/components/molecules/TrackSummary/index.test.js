import React from 'react'
import { shallow, mount } from 'enzyme'

import TrackSummary from '.'

const mockData = {
  coverUrl: 'https://is3-ssl.mzstatic.com/image/thumb/Music49/v4/d1/cd/45/d1cd453a-d19b-560c-a81f-b4a561794fd6/source/100x100bb.jpg',
  artist: 'Don Davis',
  track: 'Trinity Infinity',
  price: '1.51 $',
}

describe('TrackSummary component', () => {
  it('should render', () => {
    const wrapper = shallow(<TrackSummary {...mockData}/>);

    expect(wrapper).toMatchSnapshot();
  })

  it('should show an image with the correct source', () => {
    const wrapper = mount(<TrackSummary {...mockData}/>);

    expect(wrapper.find('img').prop('src')).toBe(mockData.coverUrl);
  })

  it('should show a list with the artist name, track name and price', () => {
    const wrapper = mount(<TrackSummary {...mockData}/>);

    expect(wrapper.find('li').length).toBe(3);
    expect(wrapper.find('li').get('0').props.children).toBe(mockData.artist);
    expect(wrapper.find('li').get('1').props.children).toBe(mockData.track);
    expect(wrapper.find('li').get('2').props.children).toEqual(mockData.price);
  })
})
